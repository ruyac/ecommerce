# E-ticaret web uygulaması hakkında #
###Katılımcılar###
* Rüya Coşkun B120910016
* Rayhan Hayraliyeva G111210091


### Kapsam ###

* 	Web uygulamasında kullanıcı arayüzü ve yönetimi paneli olmak üzere iki farklı ana modül bulunacaktır. 
* 	Kullanıcı arayüzü Responsive tasarıma sahip olup İngilizce ve Türkçe olmak üzere 2 dil destekleyecektir. 
* 	Kullanıcı arayüzü üzerinden müşteriler kayıt olabileceklerdir. 
* 	Kayıt olan müşteriler kategorilerden seçerek ürün belirleyip sepetlerine ekleyebileceklerdir. Sepete ekleme olayında miktar belirtilecektir. Ve e-ticaret normlarına göre müşteriye bilgi gösterilecektir. 
* 	Sepet birden fazla ürünü barındırabilecektir. 
* 	Ödeme seçeneği olarak havale ve kredi kartı seçenekleri ve kredi kartı taksitlendirme özellikleri bulunacaktır. 
* 	Kargo otomatik olarak hesaplanacaktır. Kargo firması belirlenebilecektir.
* 	Müşteri sipariş detayını görebilecektir. Siparişin durumu hakkında sipariş detayından bilgi alabilecektir. 
* 	Müşteri Kargo bilgilerini ve durum detaylarını da yine sipariş detayından takip edecektir. 
* 	Müşteri istediği kadar adres ekleyebilecektir. Ve Sipariş tamamlama sürecinde bu adreslerden seçebilecektir. 
* 	Müşteri kayıt olduğunda eposta onayı için kayıt olunan eposta adresine onay linki gönderilecektir. 
* 	Müşteri arama motorundan ürünün ismini ya da modelini yazarak ürüne ulaşabilecek ve benzer ürünleri asenkron zamanlı olarak süzdürdükçe görebilecektir. 
* 	Müşteri alışveriş arayüzünde kategori mantığı olacaktır. Kategoriler admin panelinden yönetilecektir. 
* 	Admin paneline standart admin şifresiyle erişim sağlandıktan sonra iştenilen rollerde alt yönetici kullanıcıları oluşturulabilecektir. 
* 	Admin Raporlar kısmında müşteri sayısını aktif sipariş sayısını ve bazı satış raporlarını görebilecektir. 
* 	Admin kategori ve alt kategori ekleyebilcektir. 
* 	Admin kategorilere ürünler ekleyebilcektir yada düzenleyebilecektir. 
* 	Ürün ekleme kısmında birden fazla ürün resmi , detayı , teknik özellikleri ve ödeme opsiyonları gibi bilgiler girilebilecektir. 
* 	Ürün temin ve kargo yönetim panelinde siparişin durumu değiştirilebilecek, kargo bilgileri ve durumu güncellenebilecektir. 
* 	Müşteriler yönetilebilecektir. 
* 	Yönetim paneli rolleri görüntülenebilecek yeni rol grupları oluşturulabilecektir. 
* 	Kullanıcı alışveriş arayüzündeki slider yönetim panelinden yönetilecektir. Ekleme çıkarma düzenleme link yönlendirme yapılacaktır. 
* 	İletişim bilgileri ve diğer detay bilgiler (footer alanında ) yine bu yönetim panelinden düzenlenecektir. 
* 	Kart taksit bilgileri ve vade oranları yine admin panelinden düzenlenebilecektir. 
* 	Sistem şimdilik sadece havale üzerinden aktif olarak satış gerçekleştirecektir. 


